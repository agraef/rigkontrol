
Rig Kontrol
===========

This is a Pd patch which translates the control (audio) output of Native
Instrument's [Guitar Rig][] Kontrol pedal to corresponding MIDI data. The
patch doesn't rely on any special externals or add-ons, so it should work with
any Pd flavor out there. Out of the box, the patch supports version 1 of the
pedal, i.e., the original [blue one][]. If you still have one of these lying
around, you can now put this nice piece of hardware to good use again as a
hands-free MIDI controller for your synthesizers, effect plugins, DAW programs
and modular systems such as Pd. And yes, you can even use it to control Guitar
Rig itself via MIDI instead of GR's built-in Kontrol interface. :)

Just to be clear: This patch **needs the hardware from NI** to do anything
useful (NI doesn't sell these any more, but you might be able to find a used
one on Ebay).

[Guitar Rig]: http://www.native-instruments.com/en/products/komplete/guitar/guitar-rig-5-pro/
[blue one]: http://www.soundonsound.com/sos/sep04/articles/niguitar.htm

Author
------

Copyright (c) 2016 by Albert Gräf from JGU Mainz <aggraef@gmail.com>

This software is provided free (as in beer) and is in the public domain, so
you can use (and modify) it for whatever purpose. There's NO WARRANTY, of
course; so if you damage your gear by using it, I don't want to hear about
it. :)

Initial Setup
-------------

This description assumes that you're already familiar with Pd. If not then you
should read a tutorial on Pd first (a great introduction to Pd is the [Pure
Data FLOSS Manual][]).

[Pure Data FLOSS Manual]: https://en.flossmanuals.net/pure-data/

The entire control applet is in the `rigkontrol` patch. After opening it in
Pd, first configure the audio input that you're going to use. As shipped, the
patch is set up to read the Rig Kontrol's input from channel 3 (usually the
left input channel of the *second* sound card). You can change this by
modifying the argument of the `riginit` subpatch accordingly. E.g., changing
it to `riginit 1` will read input from the left input channel of your first
(and primary) sound card instead. If you edited the channel number then you'll
want to save the patch to make your changes permanent.

Second, connect the *second* output of your Rig Kontrol to the audio interface
and channel that you chose above (that would be the left input channel of your
second sound card, if you keep the default channel number 3). Also make sure
that the device is switched to Ctrl mode ("Ch 2" switch on the back of the
device must be up). Use Pd's "Test Audio and MIDI" applet to verify that you
really have the device hooked up to the correct audio channel.

The Rig Kontrol device delivers a preamplified signal, so you want to use the
line input of your sound card and/or adjust input levels accordingly. Make
sure that you have a decent level from the device (not too quiet, but not
clipping either). A dB level of about 80-90 (as shown by the "Test Audio and
MIDI" applet) works fine for me, but YMMV, so you may have to experiment a
little to see what setting works best for you.

**Important:** As `rigkontrol` extracts all the control data from its audio
input, it needs a reasonably clean signal from the device to do its job. A
sure sign of a noisy input signal (assuming that the input level seems to be
ok) are switches suddenly going on and off randomly (or not at all) and
seemingly random readings from the pedal in spite of a successful
calibration. This is most often due to problems with the sound card or
cable. Check the connections, adjust the signal level, or try changing the
cable and/or the audio interface until you get a signal that works. When in
doubt, listen to the control signal while operating the device (turn the
volume down before you try this!). The frequencies of the pedal and the
different foot switches should be clearly audible, without noticeable static
or crackling noise.

Third, to hook up the `rigkontrol` patch to your DAW or MIDI gear, you also
need to set up Pd's MIDI output device(s) accordingly. By default, the patch
will output MIDI data to the first MIDI channel on the first MIDI output, but
you can change this by adjusting the "chan" control in the `rigpedal` patch
accordingly, please check the *Usage* section below for details.

By default, the four switches of the device are mapped to CC 80-83 (generic
control buttons in MIDI) and the pedal is mapped to CC 11 (expression). You
can change this as needed in the `riginit` subpatch (see the *Load-Time
Configuration* section below for details).

**Note:** Unless you're customizing the patch for other devices, there's no
reason to mess with the switch numbers 0-3 in the arguments of the `rigswitch`
subpatches. These are used internally to identify the different switches and
determine the control signal frequencies and various other parameters
associated with each switch.

Calibration
-----------

After loading the `rigkontrol` main patch in Pd, you'll see that the red
"calibrate" toggle is on, meaning that `rigkontrol` first wants you to
calibrate the pedal. This needs to be repeated every time you load the patch
in Pd, since `rigkontrol` has no a-priori knowledge about the minimum and
maximum signal levels, which are needed to correctly map the pedal signal to
the full 0-127 value range of the corresponding MIDI CC.

To calibrate, just move the pedal fully down and up, until you see the values
go from 0 to 127 in the GUI elements of the `rigpedal` subpatch. Then either
press one of the foot switches or switch "calibrate" off in the patch, and
you're ready to go. If needed, you can repeat this process at any time by just
switching "calibrate" on.

**Notes:**

* The `rigkontrol` patch actually auto-calibrates itself constantly as you're
  operating the device. The "calibrate" toggle just makes sure that no erratic
  MIDI data is generated during the initial calibration.

* There's no calibration for the switches. If these are *always* on or off or
  behave erratically, chances are that the signal level from the Rig Kontrol
  is either too high or too low, in which case you need to adjust the input
  level until the switches work as intended.

Usage
-----

Once you calibrated the device, just pushing the switches and moving the pedal
generates corresponding MIDI CC data which can be sent to your DAW, hardware
synthesizer or whatever MIDI gear you have hooked up to Pd's MIDI output. As
already mentioned, by default the first MIDI channel on the first MIDI output
will be used, but you can change this by adjusting the MIDI channel ("chan")
control in the `rigpedal` patch accordingly. (As usual, secondary MIDI outputs
can be accessed using MIDI channels above 16; e.g., MIDI channels of the
second MIDI output device are numbered 17-32, etc.)

The GUI elements in the switches and pedal subpatches give visual feedback
about the states of the hardware controls. The red toggles in the `rigswitch`
patches show the current state of the switches (on or off) and the slider and
number control in the `rigpedal` subpatch show the current value of the pedal.

In addition, you can also employ the outlets of the `rigswitch` and `rigpedal`
subpatches to send the control values directly to other Pd control objects. In
fact, you can easily embed `rigkontrol` as a ([graph-on-parent][]) subpatch in
your own patches, and the control data can then just be used internally
without going through any external MIDI devices at all. In this case, you may
want to completely switch off MIDI generation, which can be done by entering 0
as the MIDI channel number; you can also set this as default inside the
`riginit` subpatch.

[graph-on-parent]: http://en.flossmanuals.net/pure-data/ch044_graph-on-parent/

Last but not least, note that you can change the defaults for all important
settings (audio input and MIDI output channels, CC numbers, operation modes of
the foot switches, etc.) in the `riginit` subpatch, see *Load-Time
Configuration* below for details.

Switch Modes
------------

The `rigkontrol` patch actually implements three different modes in which the
switches can be operated: "stompbox", "momentary" and "program". The default
is "stompbox" mode in which the switches work like toggles, i.e., pushing the
switch alternately turns the controller on or off. In "momentary" mode the
switches work like push buttons (pushing the switch turns it on, releasing it
turns it off again immediately, which is just how the hardware actually
works). In either toggle or momentary mode the CC value 127 is emitted for
"on" and 0 for "off".

There's a third "program" mode in which the switches work like radio button
controls, i.e., activating one of the program mode switches automatically
deactivates all the others. This is typically used for switching presets or
instrument sounds and sends MIDI program changes (PC) instead of control
changes.

The switches can be assigned to each of the three modes individually by
clicking the yellow "M" and blue "P" toggles in the `rigswitch` subpatches.
The momentary ("M") and program ("P") modes are mutually exclusive. If neither
the "M" nor the "P" toggle is on then the switch operates in the default
stompbox mode.

Both the switches and the pedal can also be switched on and off individually
using the little toggle in the upper right corner of each subpatch. When a
control is switched off, it stops updating its status and does not generate
any kind of output any more until it is turned on again.

Load-Time Configuration
-----------------------

The load time defaults for all important options can be adjusted by editing
the default configuration in the `riginit` subpatch. In particular, you can
change the following settings:

- the audio channel on which `rigkontrol` should listen for the unit's control
  output; this can be changed most conveniently in the main patch by editing
  the single argument of `riginit`

- the default mode (`rigmodes`) of the switches 0-3, in that order; the three
  supported modes are denoted as 0 = stompbox, 1 = momentary, 2 = program

- the MIDI CC (`rigswitches`) and PC (`rigprograms`) numbers of the switches,
  as well as the MIDI CC (`rigpedal`) number of the pedal; you can use a MIDI
  CC of -1 (or any other negative value) to disable the corresponding control

- the default MIDI channel (`rigchannel`); a zero or negative value disables
  MIDI output

For instance, the following is `rigkontrol`'s default configuration which sets
the MIDI CCs and PCs of the switches 0-3 to 80-83 and 1-4, respectively. It
also sets all switches to stompbox mode, assigns the pedal to CC 11, and
directs MIDI output to the first MIDI channel.

    ;
    rigswitches 80 81 82 83;
    rigpedal 11;
    rigprograms 1 2 3 4;
    rigmodes 0 0 0 0;
    rigchannel 1

In the `riginit` patch you'll also find various sample alternative
configurations which you can try by just clicking on them. Have a look at
these examples to get an idea on how to create your own configurations. Of
course, you can customize all of these items to your heart's content in order
to tailor `rigkontrol` to your needs.

Rig Kontrol v2 and v3
---------------------

The present version of the patch is tailored to version 1 of the Kontrol
device, since that's the only version I have for testing. At least some of the
switches and the pedal might also work with newer (v2 and v3) versions of the
device, but I haven't tested this, so you're on your own.

Adjusting the patch to the later versions should be a piece of cake,
though. Have a look at the `rigswitch` and `rigpedal` subpatches, figure out
what the frequencies of the control signals for the switches and pedal of your
version of the device are, and modify the filter frequencies and/or amplitude
thresholds in the subpatches accordingly. You'll also need to adjust the
number of `rigswitch` instances in the main patch, as the Kontrol v2 has six
and the Kontrol v3 eight foot switches. It goes without saying that if you
manage to get a v2 or v3 device working, then it would be nice if you could
contribute the necessary changes.

If you're not into Pd programming but have a spare Kontrol v2 or v3 device
that you're willing to donate so that I can add support for these devices
myself, then please get in touch with me at <aggraef@gmail.com>.


Enjoy! :)

Albert Gräf <aggraef@gmail.com>
